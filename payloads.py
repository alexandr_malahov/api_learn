from api_learn.utilities.configurations import get_query
import random

def payload(isbn,aisle):
    return {
        "name": "Learn Appium Automation with Javaa",
        "isbn": isbn,
        "aisle": aisle,
        "author": "John foje"
    }





def build_payload(query):
    add_book = {}
    tp = get_query(query)
    add_book['name'] = tp[0]
    add_book['isbn'] = tp[1]+str(random.randint(1, 100))
    add_book['aisle'] = tp[2]
    add_book['author'] = tp[3]
    return add_book
