import configparser
import mysql.connector


def getconfiguration():
    config = configparser.ConfigParser()
    config.read('api_learn/utilities/properties.ini')
    return config


connect_conf = {'user': getconfiguration()['SQL']['user'],
                'password': getconfiguration()['SQL']['password'],
                'host': getconfiguration()['SQL']['host'],
                'database': getconfiguration()['SQL']['database']
                }


def get_connection():
    try:
        conn = mysql.connector.connect(**connect_conf)
        if conn.is_connected():
            print('Connected to MySQL database')
            return conn
    except mysql.connector.Error as e:
        print(e)


def get_query(query):
    conn = get_connection()
    cursor = conn.cursor()
    cursor.execute(query)
    row = cursor.fetchone()
    conn.close()
    return row

