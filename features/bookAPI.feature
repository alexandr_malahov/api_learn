Feature: Verify if book is added and deleted using api
  @Smoke
  Scenario: Verify book to the DB
    Given the book details which needed to be added to library
    When execute the addbook postapi method
    Then the book successfully added


  @Regression
  Scenario Outline: Verify addbook api functionality
    Given the book details <isbn> and <aisle>
    When execute the addbook postapi method
    Then the book successfully added
    Examples:
      |isbn    | aisle  |
      | test11 | 1911    |
      | test22 | 2202    |