from behave import *

from api_learn.payloads import build_payload, payload
# from api_learn.utilities.resourses import *
from api_learn.utilities.configurations import *
# from api_learn.payloads import *
import requests

from api_learn.utilities.resourses import ApiResources


@given('the book details which needed to be added to library')
def step_impl(context):
    context.url = getconfiguration()['API']['endpoint'] + ApiResources.addBook
    context.headers = {"Content-Type": "application/json"}
    context.payload = payload(context.isbn, context.aisle)


@when('execute the addbook postapi method')
def step_impl(context):
    context.addBook_response = requests.post(context.url, json=context.payload, headers=context.headers)


@then('the book successfully added')
def step_impl(context):
    print(context.addBook_response.json())
    response_json = context.addBook_response.json()
    print(type(response_json))

    context.bookId = response_json['ID']
    print(f' Search the next ID {context.bookId} in the database')
    assert response_json['Msg'] == 'successfully added'


@given('the book details {isbn} and {aisle}')
def step_impl(context, isbn, aisle):
    context.url = getconfiguration()['API']['endpoint'] + ApiResources.addBook
    context.headers = {"Content-Type": "application/json"}
    context.payload = payload(isbn, aisle)
