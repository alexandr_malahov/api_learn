import json

import requests

URL = 'http://216.10.245.166/Library/GetBook.php'

r = requests.get(URL, params={'AuthorName': 'Rahul Shetty'})
#print(r.json())
assert r.status_code == 200
assert r.headers['content-type'] == 'application/json;charset=UTF-8'
print(r.cookies)

for actual_book in r.json():
    if actual_book['isbn'] == 'RGHCC':
        print(actual_book)
        break

assert actual_book['book_name'] == 'Learn with Java'