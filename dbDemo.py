import mysql.connector
from utilities.configurations import *
# conn = mysql.connector.connect(host='localhost', database='apidevelop', user='root', password='Root123!')

conn = get_connection()

print(conn.is_connected())


cursor = conn.cursor()

# query = "update CustomerInfo set Location=%s where CourseName = %s"
# data = ('Montenegro', 'Jmeter')
# cursor.execute(query, data)
# # cursor.execute("update CustomerInfo set Location='USA' where Amount = 76")
# conn.commit()
# print(cursor.rowcount, "record(s) affected")
cursor.execute("select * from CustomerInfo")
rows = cursor.fetchall()
sum = 0
for row in rows:
    print(row[2])
    sum = sum + row[2]


print("Sum of all the amounts is: ", sum)

assert sum == 340, 'wrong number'
conn.close()